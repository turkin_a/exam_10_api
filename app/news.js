const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage: storage});
const router = express.Router();

const createRouter = db => {
  router.get('/', (req, res) => {
    db.query('SELECT * FROM `news`', (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  router.get('/:id', (req, res) => {
    db.query('SELECT * FROM `news` WHERE id=' + req.params.id, (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  router.post('/', upload.single('image'), (req, res) => {
    if (req.body.title && req.body.description) {
      const post = req.body;
      post.datetime = new Date();

      if (req.file) {
        post.image = req.file.filename;
      }

      db.query('INSERT INTO `news_db`.`news` (`title`, `description`, `image`, `datetime`)' +
      `VALUES ('${post.title}', '${post.description}', '${post.image}', '${post.datetime}')`,
      (error, results) => {
        if (error) throw error;
        res.send(results);
      });
    } else {
      res.status(400).send({error: 'Content of post is empty'});
    }
  });

  router.delete('/:id', (req, res) => {
    db.query('DELETE FROM `news` WHERE id=' + req.params.id, (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  return router;
};

module.exports = createRouter;