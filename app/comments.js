const express = require('express');

const router = express.Router();

const createRouter = db => {
  router.get('/', (req, res) => {
    db.query('SELECT * FROM `comments` WHERE news_id=' + req.query.news_id, (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  router.post('/', (req, res) => {
    if (req.body.description) {
      const comment = req.body;
      if (!comment.author) comment.author = 'Anonymous';

      db.query('INSERT INTO `news_db`.`comments` (`news_id`, `author`, `description`)' +
        `VALUES ('${comment.newsId}', '${comment.author}', '${comment.description}')`,
        (error, results) => {
          if (error) throw error;
          res.send(results);
        });
    } else {
      res.status(400).send({error: 'Comment is empty'});
    }
  });

  router.delete('/', (req, res) => {
    db.query('DELETE FROM `comments` WHERE id=' + req.query.id, (error, results) => {
      if (error) throw error;
      res.send(results);
    });
  });

  return router;
};

module.exports = createRouter;