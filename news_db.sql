DROP DATABASE IF EXISTS `news_db`;
CREATE DATABASE `news_db`;

CREATE TABLE `news_db`.`news` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(255) NOT NULL,
    `description` TEXT NOT NULL,
    `image` VARCHAR(255),
    `datetime` VARCHAR(45) NOT NULL
);

CREATE TABLE `news_db`.`comments` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `news_id` INT NOT NULL,
    `author` VARCHAR(255),
    `description` TEXT NOT NULL,
    INDEX `FK_news_idx` (`news_id`),
    CONSTRAINT `FK_news`
		FOREIGN KEY (`news_id`)
        REFERENCES `news_db`.`news` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);